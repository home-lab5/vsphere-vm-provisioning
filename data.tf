data "vsphere_datacenter" "dc" {
  name = "ITNOOBS"
}

data "vsphere_datastore" "datastore" {
  name          = "vsanDatastore"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = "Mitcham"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = "VLAN2 - Core"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "is_datastore" {
  name          = "iso"
  datacenter_id = data.vsphere_datacenter.dc.id
}