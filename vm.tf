
resource "vsphere_virtual_machine" "vm" {
  name                 = var.vmname
  datastore_id = data.vsphere_datastore.datastore.id
  

  num_cpus = 2
  memory   = 2048
  guest_id = "ubuntu64Guest"

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  disk {
    label = "disk0"
    size  = 10
    thin_provisioned = true
  }

  cdrom {
    datastore_id = data.vsphere_datastore.iso_datastore.id
    path         = "ubuntu-20.04.1-live-server-amd64.iso"
  }

  cdrom {
    datastore_id = data.vsphere_datastore.iso_datastore.id
    path         = "preseed.iso"
  }
}



#guest_id https://code.vmware.com/apis/1067/vsphere