variable "vmname" {
    type =  string
    default = "itnoobs-linux"
}

variable "vmcount" {
    type = string
    default = "2"
}